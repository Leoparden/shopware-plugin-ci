
ARG SW_VER
# Set the environment variable with the argument value
FROM ghcr.io/friendsofshopware/platform-plugin-dev:v${SW_VER} AS base

ARG SW_VER
ENV SW_VER=${SW_VER}
ENV NODE_PACKAGE_URL=https://unofficial-builds.nodejs.org/download/release/v20.9.0/node-v20.9.0-linux-x64-musl.tar.gz
RUN apk add --update linux-headers
RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN apk del -f .build-deps

RUN wget $NODE_PACKAGE_URL
RUN mkdir -p /opt/nodejs
RUN tar -zxvf *.tar.gz --directory /opt/nodejs --strip-components=1
RUN rm *.tar.gz
RUN ln -s /opt/nodejs/bin/node /usr/local/bin/node
RUN ln -s /opt/nodejs/bin/npm /usr/local/bin/npm

# Ensure $SW_VER can be used in the RUN command
RUN echo "SW_VER is set to $SW_VER" && \
    case "$SW_VER" in \
        *6.4*) \
            echo "SW_VER contains 6.4"; \
            npm i --prefix ${SHOPWARE_BUILD_DIR}/vendor/shopware/storefront/Resources/app/storefront; \
            npm i --prefix ${SHOPWARE_BUILD_DIR}/vendor/shopware/administration/Resources/app/administration; \
            ;; \
        *6.6*) \
            echo "SW_VER contains 6.6"; \
            composer init:js \
            ;; \
        *) \
            echo "SW_VER does not contain 6.4"; \
            npm i --prefix ${SHOPWARE_BUILD_DIR}/src/Storefront/Resources/app/storefront; \
            npm i --prefix ${SHOPWARE_BUILD_DIR}/src/Administration/Resources/app/administration; \
            ;; \
    esac
ENV XDEBUG_MODE=coverage
ENV DISABLE_COVERAGE=0
ENV DISABLE_PHP_TEST=0
ENV DISABLE_ADMINISTRATION_TEST=0
ENV DISABLE_STOREFRONT_TEST=0
ENV DISABLE_DOCS=0
ENV DISABLE_PUBLISH=0
ENV DISABLE_CS=0
ENV PLUGIN_NAME="PLUGIN"

COPY leoparden_plugin_ci /usr/local/bin/docker-entrypoint.sh
COPY .phpunit-coverage-threshold.php /usr/local/bin/coverage-threshold
ENTRYPOINT ["docker-entrypoint.sh"]
