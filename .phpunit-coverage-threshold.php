#!/usr/bin/env php
<?php
/*
 * Copyright (c) 2024. Die Leoparden GmbH
 */
try {
    if (3 != $argc) {
        echo 'Usage: '.$argv[0].' <path/to/index.xml> <threshold>
';
        exit(-1);
    }

    $file = $argv[1];
    $threshold = (float) $argv[2];

    $coverage = simplexml_load_file($file);
    if ($coverage) {
        $ratio = (float) $coverage->project->directory->totals->lines['percent'];

        echo "Line coverage: $ratio%
";
        echo "Threshold: $threshold%
";

        if ($ratio < $threshold) {
            echo 'FAILED!
';
            exit(-1);
        }

    } else {
        echo 'some internal error is happened. Continuing.';
    }
        echo 'SUCCESS!
';
} catch (Exception) {
    echo 'some internal error is happened. Continuing.';
}
