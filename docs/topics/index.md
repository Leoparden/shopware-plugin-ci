# Testing und Deployment?

## Testing!

Nur getesteter Code ist wirklich guter Code. Denn nur so kann ein Entwickler nachweislich behaupten, dass der
geschriebene Code funktioniert wie zu erwarten. Alles darunter ist in den Augen eines Aussenstehenden lediglich "gut
geraten".
Um dem vorzugreifen, ist Testing wichtig.

Es gibt verschiedene Entwicklungs- und Designparadigmen. Für die Entwicklung eines Plugins kommen aber an dieser Stelle
wirklich nur zwei in Frage.
 
<tabs>
<tab title="Test Driven Development"> 
    Einfach ausgedrückt umfasst der TDD-Prozess das Schreiben automatisierter Tests vor dem Schreiben des Codes. Die Ergebnisse dieser automatisierten Tests liefern dem Entwickler Erkenntnisse zur Verbesserung seines Codes. TDD ist ein fokussierterer und disziplinierterer Entwicklungsansatz und stellt selbst eine Möglichkeit dar, kontinuierliches Feedback für eine schnellere Fehlererkennung und Fehlerbehebung bereitzustellen.

Entwickler starten den TDD-Prozess, indem sie einen Test schreiben. In diesem Test definieren sie speziell das erwartete
Verhalten oder die erwartete Funktionalität einer kleinen Codeeinheit, die Folgendes umfassen kann:

- Code-Eingabe (welche Parameter der Code erhält)
- Codeausgabe
- Voraussetzungen/Abhängigkeiten, die für die Testausführung vorhanden sein müssen
- Behauptungen, auch genannt "Assertions"

Im nächsten Schritt wird der Test ausgeführt (Am besten im Watch-Mode des spezifischen Testing Framework), der
offensichtlich fehlschlagen wird.

Im weiteren Verlauf wird die Funktionalität implementiert. Durch kontinuierliches "Feintuning" des Codes wird der Test
im Laufe der Entwicklung irgendwann auch mit positiver Meldung abschließen.

Der Vorteil an dieser Vorgehensweise ist, dass es annähernd ausgeschlossen ist ungetesteten Code zu schreiben. Der Code
wird genau das tun, was initial durch den Test definiert wurde - nicht mehr und nicht weniger. Dadurch kann man einen
geringeren Overhead und eine fast Lückenlose Coverage erreichen ohne größer darüber nachdenken zu müssen, wie der Code
am Ende getestet werden kann.

</tab>
<tab title="Agile Development">
    <tip>
        <p>nicht zu verwechseln mit "Agile Projectmanagement"</p>
    </tip>
    Ganz einfach gesagt: "Drauflos coden und nachher testen"<br/>
    Der Vorteil ist, dass ein Softwareprodukt schnell einen MVP Stand erreichen kann und der Kunde/Project-Owner selbst in erster Linie der Tester darstellt.
    Mit Agile Development kann wie der Name schon sagt, Agil - Schnell gearbeitet werden. Dies verhält sich dann aber wie bei ein Handwerker, der "schnell-schnell" 
    etwas fertig macht, dann aber fast dieselbe Zeit noch einmal braucht um seine Fehler auszubügeln, noch einmal nachzumessen und hinter sich aufzukehren. <br/>
    Diese "Ausbesserungszeit" lässt sich meist gegenüber dem Kunden/Project-Owner nur schwer rechtfertigen. Dennoch kann diese Vorgehensweise für sogenanntes 
    "Rapid-Prototyping" mehr Vorteile als Nachteile bringen.  
</tab>
</tabs>

## Deployment!

Deployment zu Kunden kann auf verschiedene Wege passieren.

- Direkter und ständiger Upload während der Entwicklung
- Packaging und weitestgehend ständiger Upload
- Repository-Bridging ohne Versionierung (git pull auf Server / Composer Update auf speziellen Branches)
- Repository-Bridging mit Versionierung (git pull auf Server / Composer Update auf speziellen Tags)
- Deployment zu Public (Shopware-Store)
- Package-Registry Deployment (Packagist / Private-Registry (gitlab))

Nun ist die Problematik bei einigen der Oben genannten Punkte offensichtlich, bei anderen aber weniger.
Repository-Bridging und Direkter Upload kann zu unvorhergesehenen Bugs führen, weshalb diese Vorgehensweise nur mit
bedacht ausgeführt werden sollte.

Das Deployment zu Public Stores kommt mit der Problematik, dass der generelle Ansatz dort immer "Open Source" ist. Dies
ist im Grunde nichts Schlechtes,
aber in manchen Situationen für uns eher mit einem Beigeschmack behaftet.

Mit einer privaten "Package-Registry" haben wir die volle Kontrolle über das Deployment und Lizenzierung von
Erweiterungen. Mit einem CI Prozess, der
direkt bei einem Versuch einen "Tag" zu erstellen das komplette Repository testet und die Registrierung des Plugins in
der Package-Registry übernimmt
hat ein Entwickler die Zeit sich um "die wichtigen Dinge" zu kümmern. Coden & Testen!
