---
switcher-label: Autorisierungsmethode
---
# Deployment
<p>Die Integration in ein Kundenprojekt erfolgt zuerst über die Freigabe des Servers in dem entsprechenden Repository und
der Package-Registry.</p>
<p>
Dies kann über einen Deployment-Token oder über Deployment-Keys geschehen. Zur jeweiligen Anleitung den Switcher in der Top-Bar nutzen.
</p>
<tip>WMS-Shops werden automatisch registriert.</tip>

## Deployment-Token { switcher-key="Deployment-Token" }
<a href="https://gitlab.com/DieLeoparden/shopware/%pluginrepo%/-/settings/repository#js-deploy-tokens">Gitlab-Deployment Token Settings für %pluginname%</a>

Die Authentifizierung wird von Composer übernommen. Hierfür muss die Package-Registry mit den entsprechenden Authentifizierungsdaten in der Composer-Instanz registriert werden.
<code-block lang="bash">
    composer config gitlab-token.gitlab.com %access_user% %access_token%
    composer config repositories.gitlab.com/%shopware_group_id% '{"type": "composer", "url": "https://gitlab.com/api/v4/group/%shopware_group_id%/-/packages/composer/packages.json"}'
</code-block>
Für die einzelnen Repositories dann respektiv dessen tokens
<code-block lang="bash">
composer require https://%access_user%:%access_token%@gitlab.com/DieLeoparden/shopware/%pluginrepo%.git
</code-block>

## Deployment-Key { switcher-key="Deployment-Key" }
<a href="https://gitlab.com/DieLeoparden/shopware/%pluginrepo%/-/settings/repository#js-deploy-keys-settings">Gitlab-Deployment Keys Settings für %pluginname%</a>

Zum erzeugen eines Key-Pairs, wenn auf dem Server noch kein Public Key erzeugt wurde
<code-block lang="bash">
    ssh-keygen -t ed25519 -C "Gitlab Deployment Key" -f ~/.ssh/gitlab-deployment
    cat ~/.ssh/gitlab-deployment
</code-block>
Der ausgegebene Key muss dem Repository unter "New deploy key" hinzugefügt werden.
Der Name des Keys sollte immer der Name des entsprechenden Kunden sein, um spätere Verwirrung zu vermeiden. Wenn der Kunde mehrere Server besitzt, sollten die Instanzen auch entsprechend benannt werden. 

Auf dem Server kann dann die Registry hinzugefügt werden
<code-block lang="bash">
    composer config repositories.gitlab.com/%shopware_group_id% '{"type": "composer", "url": "https://gitlab.com/api/v4/group/%shopware_group_id%/-/packages/composer/packages.json"}'
</code-block>