# Storefront

Testing von Shopware-Storefront Modulen stellt sich dar wie bei jedem anderen Javascript/Typescript Projekt.

Gesetz dem Falle natürlich, man verwendet keine weiteren dependencies die das Testen mit konventionellen mitteln als Schwierig darstellt (z.B. Vue)

Für das Testing sind natürlich weitere Abhängigkeiten notwendig:

<code-block lang="json" src="storefront.package.json" include-lines="1-18,26-27" collapsible="true" collapsed-title="<plugin-root>/src/Resources/app/storefront/package.json">

</code-block>

Sollte Vue verwendet werden durch die Nutzung von Komponenten aus dem BasePlugin-Storefront, kommen noch weitere Elemente hinzu

<code-block lang="json" src="storefront.package.json" include-lines="1,6,21-27" collapsible="true" collapsed-title="<plugin-root>/src/Resources/app/storefront/package.json">

</code-block>

Um Jest mit Typescript und Klassen verwenden zu können, wird Babel entsprechend konfiguriert:

<code-block lang="javascript" src="storefront.babel.config.js" include-lines="5-" collapsible="true" collapsed-title="<plugin-root>/src/Resources/app/storefront/babel.config.js">

</code-block>

Jest muss natürlich auch noch Konfiguriert sein:

<code-block lang="javascript" src="storefront.jest.config.js" include-lines="5-" collapsible="true" collapsed-title="<plugin-root>/src/Resources/app/storefront/jest.config.js">

</code-block>

Manche Elemente, die von Shopware geliefert werden, werden anhand der initialisierungsdatei von BasePlugin/Storefront schon erledigt. Man kann aber auch in folgender Datei weiteres Anpassen

<code-block lang="typescript" src="storefront.jest.init.ts" include-lines="5-" collapsible="true" collapsed-title="<plugin-root>/src/Resources/app/storefront/jest.init.js">

</code-block>

Zuletzt muss der Typescript Compiler noch ein paar wenige Einstellungen erhalten, damit zum einen Code-Highlighting in PHP-Storm korrekt funktioniert als auch Jest die korrekten Pfade kennt.

<code-block lang="json" src="storefront.tsconfig.json" include-lines="5-" collapsible="true" collapsed-title="<plugin-root>/src/Resources/app/storefront/tsconfig.json">

</code-block>

Sollte das BasePlugin nur als Dependency installiert worden sein, so muss natürlich der Pfad entsprechend auf ```<project_root>/vendor/leoparden/baseplugin/src/Resources/app/storefront/<file>``` zeigen


Mit ```npm install``` und ```npm run test-watch``` kann nun das Testen beginnen.

Ein Beispiel-Test:

<code-block lang="typescript" src="storefront.spec.ts" include-lines="5-" collapsible="true" collapsed-title="<plugin-root>/src/Resources/app/storefront/test/storefront.spec.ts">

</code-block>

