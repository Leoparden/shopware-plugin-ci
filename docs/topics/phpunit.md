# PHPunit

Für die Ausführung von PHPunit Tests sind nur wenige Dateien wirklich notwendig.

Der Start ist immer die Konfiguration des Testing Frameworks:

<code-block src="phpunit.xml" lang="xml" include-lines="5-46" collapsed-title="<plugin-root>/phpunit.xml" collapsible="true" default-state="collapsed">

</code-block>

Für PHP-Unit Tests ist aber auch immer ein Bootstrapper notwendig, der dem Framework die Rahmenbedingungen des Tests mitteilt. In unserem Falle haben wir den Vorteil, dass Shopware dort schon die meiste Arbeit gemacht hat und wir deren Bootstrapper einfach verwenden können:

<code-block src="TestBootstrap.php" lang="php" collapsed-title="<plugin-root>/TestBootstrap.php" include-lines="1,6-21"  collapsible="true" default-state="collapsed">

</code-block>

Die PhpUnit tests werden über das von Shopware mitgelieferte Package "phpunit" getestet.

<code-block lang="bash">
$SHOPWARE_ROOT/vendor/bin/phpunit -c custom/plugins/%pluginname%/phpunit.xml
</code-block>

Mit der oben genannten Konfiguration werden Coverage und Report Ordner erstellt. Diese dann am besten in die ```<plugin-root>/.gitignore``` Datei aufnehmen
Um über den aktuellen Stand der Coverage im Bilde zu sein, kann die ```<plugin-root>/coverage/html-coverage/index.html``` Datei im Browser geöffnet werden und entsprechend nach durchgelaufenen Tests aktualisiert werden.  


## Verfassen eines Tests

Ein Unit-Test besteht immer aus den Teilen:
 - Set-Up
 - Test
 - Tear-down

Der Setup und Tear-down Teil wird zum großen Teil von dem IntegrationTestBehaviour Trait übernommen.

Das Testing aber, kommt vom Entwickler. Der Test selbst sollte so kleinteilig wie möglich testen. Eine Funktion - ein Test

Im Nachfolgenden Code wird zum Beispiel geprüft, ob das Plugin erfolgreich installiert wurde. Bei Plugins mit Datenbanktabellen sollte hier also auch geprüft werden, ob die Tabellen existieren und Nutzbar sind.

<code-block lang="php" src="YourPluginNameTest.php" collapsed-title="<plugin-root>/Test/%pluginname%Test.php" include-lines="7-32" collapsible="true" default-state="collapsed">

</code-block>
