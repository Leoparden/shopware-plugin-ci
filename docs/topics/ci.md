# CI

<p>
  Das integrieren des CI-Prozess an sich ist sehr simpel. Alles, was dafür zusätzlich getan werden muss, ist folgende
  datei in das Projekt mitaufzunehmen
</p>

<code-block src=".gitlab-ci.yml" lang="yaml" collapsible="true" default-state="collapsed" collapsed-title="<plugin-root>/.gitlab-ci.yml">

</code-block>

## Lokales Testen der CI

Um den Deployment-Prozess lokal zu testen, gibt es die möglichkeit via Docker das FroSh-Dev-Image lokal zu verarbeiten.

Dafür wird zum einen eine DotEnv Datei sowie ein kleines Script benötigt:

<code-block src=".env" lang="shell" collapsible="true" default-state="collapsed" collapsed-title="<plugin-root>/.env">

</code-block>

Variablen:

- Der Registry Token und Registry User sind in wenn in der Shopware Gruppe hochgeladen wird automatisch definiert, hier
  muss dann aber ein eigener Token definiert werden zur authentifizierung des Docker-Image zum Gitlab-Server
- Pluginname und dessen hochgestellte Variante muss definiert werden, da sonst ein erfolgreicher Build nicht möglich
  ist. Hierbei einfach den Namen des Plugin selbst verwenden.

Das Script zur ausführung des Docker-Image

<code-block src="run_docker.sh" lang="shell" collapsible="true" default-state="collapsed" collapsed-title="run-docker">

</code-block>

Zur einfachen Ausführung dessen am besten in /usr/bin/run_docker (oder anderer, bevorzugter Name) kopieren und per

<code-block lang="shell">
chmod+x /usr/bin/run_docker
</code-block>
ausführbar machen

Die Ausführung des Scripts erfolgt über den Befehl

<code-block lang="shell">
run_docker "$(realpath .)"
</code-block>

## Log-Output

Eine beispielhafte Ausgabe des Scripts:
<code-block lang="console" src="log" collapsible="true" default-state="collapsed" collapsed-title="log">

</code-block>