//@ts-nocheck
/*
 * Copyright (c) 2024. Die Leoparden GmbH
 */

export {};
declare global {
  namespace EntitySchema {
    interface Entities {
      leoparden_customer_contact_type: leoparden_customer_contact_type;
      leoparden_customer_contact_type_translation: leoparden_customer_contact_type_translation;
      leoparden_customer_contact_person: leoparden_customer_contact_person;
      leoparden_customer_contact_person_translation: leoparden_customer_contact_person_translation;
    }

    interface leoparden_customer_contact_person_translation
      extends TranslationEntity {
      description: string;
    }

    interface leoparden_customer_contact_type_translation
      extends TranslationEntity {
      name: string;
    }

    interface leoparden_customer_contact_person
      extends TranslatableEntity<"leoparden_customer_contact_person_translation"> {
      id: IdString;
      typeId: IdString;
      type: Entity<"leoparden_customer_contact_type">;
      mediaId: IdString;
      media: Entity<"media">;
      firstname: string;
      title: string;
      active: string;
      lastname: string;
      shortcode: string;
      name: string;
      phone: string;
      email: string;
    }

    interface leoparden_customer_contact_type
      extends TranslatableEntity<"leoparden_customer_contact_type_translation"> {
      id: IdString;
      contacts: EntityCollection<"leoparden_customer_contact_person">;
    }
  }
}
