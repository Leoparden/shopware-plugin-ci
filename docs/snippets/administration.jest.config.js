/*
 * Copyright (c) 2024. Die Leoparden GmbH
 */
const { resolve, join } = require("path");

process.env.ADMIN_PATH =
  process.env.ADMIN_PATH ||
  resolve(
    "../../../../../../../vendor/shopware/administration/Resources/app/administration"
  );

process.env.BASEPLUGIN_PATH =
  process.env.BASEPLUGIN_PATH || resolve("../../../../../BasePlugin");

module.exports = {
  preset: "@shopware-ag/jest-preset-sw6-admin",
  globals: {
    adminPath: process.env.ADMIN_PATH,
  },
  coverageThreshold: {
    global: {
      branches: 50,
      functions: 90,
      lines: 90,
      statements: 90,
    },
  },
  reporters: [
    "default",
    ["jest-junit", { outputDirectory: "reports", outputName: "report.xml" }],
  ],

  setupFilesAfterEnv: [
    resolve(
      join(
        process.env.BASEPLUGIN_PATH,
        "/src/Resources/app/administration/test/jest.init.ts"
      )
    ),
  ],

  collectCoverageFrom: [
    "<rootDir>/src/**/*.ts",
    "!<rootDir>/src/**/*.spec.ts",
    "!<rootDir>/src/**/*.d.ts",
  ],

  testMatch: ["<rootDir>/**/*.spec.ts"],
  transform: {
    ".*\\.(svg)$": `${process.env.ADMIN_PATH}/test/transformer/svgStringifyTransformer.js`,
  },
  transformIgnorePatterns: [
    "/node_modules/(?!(@shopware-ag/meteor-icon-kit|uuidv7|other)/)",
  ],

  moduleNameMapper: {
    "^uuid$": require.resolve("uuid"),
    "^@shopware-ag/admin-extension-sdk/es/(.*)": `${process.env.ADMIN_PATH}/node_modules/@shopware-ag/admin-extension-sdk/umd/$1`,
    "^@LeopardenBasePlugin(.*)$": `${process.env.BASEPLUGIN_PATH}/src/Resources/app/storefront/src$1`,
    vue$: "vue/dist/vue.common.dev.js",
  },
};
