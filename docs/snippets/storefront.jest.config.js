/*
 * Copyright (c) 2024. Die Leoparden GmbH
 */

const { resolve } = require("path");
process.env.STOREFRONT_PATH =
  process.env.STOREFRONT_PATH ||
  resolve(
    "../../../../../../../vendor/shopware/storefront/Resources/app/storefront"
  );

process.env.BASEPLUGIN_PATH =
  process.env.BASEPLUGIN_PATH || resolve("../../../../../BasePlugin");

module.exports = {
  preset: "ts-jest",
  transform: {
    "^.+\\.(ts|tsx)?$": "ts-jest",
    "^.+\\.(js|jsx)$": "babel-jest",
  },
  testEnvironment: "jsdom",
  globals: {
    storefrontPath: process.env.STOREFRONT_PATH,
  },

  coverageThreshold: {
    global: {
      branches: 50,
      functions: 90,
      lines: 90,
      statements: 90,
    },
  },
  reporters: [
    "default",
    ["jest-junit", { outputDirectory: "reports", outputName: "report.xml" }],
  ],

  collectCoverage: true,

  coverageReporters: ["lcov", "text", "clover"],
  testRegex: "/test/.*\\.(test|spec)?\\.(ts|tsx)$",
  collectCoverageFrom: [
    "<rootDir>/src/**/*.ts",
    "!<rootDir>/src/**/*.spec.ts",
    "!<rootDir>/src/**/*.d.ts",
  ],
  moduleDirectories: ["node_modules"],
  moduleNameMapper: {
    "^bootstrap(.*)$": `${process.env.STOREFRONT_PATH}/node_modules/bootstrap$1`,
    "^src(.*)$": `${process.env.STOREFRONT_PATH}/src/$1`,
    "^@LeopardenBasePlugin(.*)$": `${process.env.BASEPLUGIN_PATH}/src/Resources/app/storefront/src$1`,
  },

  testEnvironmentOptions: {
    customExportConditions: ["node", "node-addons"],
  },

  setupFilesAfterEnv: [
    `${process.env.STOREFRONT_PATH}/jest.init.js`,
    `${process.env.BASEPLUGIN_PATH}/src/Resources/app/storefront/test/jest.init.ts`,
  ],
};
