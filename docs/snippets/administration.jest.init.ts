//@ts-nocheck
/*
 * Copyright (c) 2024. Die Leoparden GmbH
 */

window._features_ = {
  VUE3: false,
  vue3: false,
};

global.console = {
  ...console,
  // uncomment to ignore a specific log level
  // log: jest.fn(),
  debug: jest.fn(),
  info: jest.fn(),
  warn: jest.fn(),
  // error: jest.fn(),
};
