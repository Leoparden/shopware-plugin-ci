/*
 * Copyright (c) 2024. Die Leoparden GmbH
 */
import %pluginname%Plugin from "../src/plugin/%pluginname%.plugin";
import { mount } from "@vue/test-utils";
import { resolve } from "path";
import Pagination from "@LeopardenBasePlugin/lib/Pagination";
import { nextTick } from "vue";

describe("%pluginname%", () => {
  let plugin: %pluginname%Plugin, app;

  const onResponse = (): Promise<EntityResponse<{id: string, productNumber:string}>> =>
    new Promise((resolve) => {
      plugin.$emitter.subscribe(
        "onResponse",
        async () => {
          await nextTick();
          resolve(<EntityResponse<{id: string, productNumber:string}>>plugin.response);
        },
        { once: true }
      );
    });

  const setData = async (): Promise<EntityResponse<{id: string, productNumber:string}>> => {
    const waiter = onResponse();
    plugin.refresh();
    return await waiter;
  };

  const options = {
    url: resolve("./test/fixtures/product.json"),
    sorting: {
      field: "productNumber",
    },
    baseUrl: "http://www.test.com",
  };

  beforeEach(async () => {
    window.PluginManager.register(
      %pluginname%Plugin.pluginName,
      %pluginname%Plugin,
      %pluginname%Plugin.selector
    );
    const element = document.createElement("div");
    element.dataset.%pluginname% = "true";
    element.dataset.%pluginname%Options = JSON.stringify(options);

    document.body.appendChild(element);
    const init: Promise<%pluginname%Plugin> = new Promise((resolve) => {
      element.addEventListener("onInit", (event: CustomEvent) =>
        resolve(event.detail)
      );
    });

    new %pluginname%Plugin(element, options, %pluginname%Plugin.pluginName);
    plugin = await init;

    plugin.pagination = new Pagination(plugin);
    app = mount({
      ...plugin.appOptions(plugin),
      template:
        "<div>{{response?.elements}}{{plugin.domain}}<table><tr v-for='product in response?.elements'><td :data-id='product.id'><a :href='plugin.getURL(product)'>{{product.name}}</a></td></tr></table></div>",
    });
  });

  it("should be a vue component", () => {
    expect(app.vm).toBeTruthy();
  });

  it("should load products", async () => {
    const response = await setData();
    expect(plugin.categories).toBeInstanceOf(Map);

    expect(plugin.getURL(plugin.categories.get("1").products[0])).toEqual(
      `${options.baseUrl}/${response.elements[0].seoUrls[0].pathInfo}`
    );
    expect(plugin.getURL(plugin.categories.get("1").products[1])).toEqual(
      `${options.baseUrl}/detail/${response.elements[1].id}`
    );
  });
});
