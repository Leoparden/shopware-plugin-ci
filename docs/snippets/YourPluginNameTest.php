<?php
/**
 * Leoparden\%pluginname%
 * Copyright (c) Die Leoparden GmbH
 */

namespace Leoparden\%pluginname%\Test;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Plugin\PluginService;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Shopware\Core\Framework\Test\TestCaseBase\IntegrationTestBehaviour;
use Leoparden\%pluginname%\%pluginname%;

#[CoversClass(%pluginname%::class)]
class %pluginname%Test extends TestCase
{
    use IntegrationTestBehaviour;

    public function getName(bool $withDataSet = true): string
    {
        return '%pluginname%Test';
    }

    #[Test]
    public function IsInstalled(): void
    {
        $plugin = $this->getContainer()->get(PluginService::class)->getPluginByName('%pluginname%', Context::createDefaultContext());
        $this->assertInstanceOf(\DateTimeInterface::class, $plugin->getInstalledAt());
    }
}
