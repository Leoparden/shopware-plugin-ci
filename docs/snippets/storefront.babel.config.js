/*
 * Copyright (c) 2024. Die Leoparden GmbH
 */

module.exports = {
  presets: ["@babel/preset-env"],
  plugins: [
    "@babel/plugin-transform-class-properties",
    "@babel/plugin-transform-runtime",
  ],
};
